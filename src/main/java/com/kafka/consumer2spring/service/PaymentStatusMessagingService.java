package com.kafka.consumer2spring.service;

import com.kafka.consumer2spring.dto.response.PaymentStatusMessageResponseDTO;

import java.util.List;

public interface PaymentStatusMessagingService {

    List<PaymentStatusMessageResponseDTO> getMessages();

    void clearCache();

}
