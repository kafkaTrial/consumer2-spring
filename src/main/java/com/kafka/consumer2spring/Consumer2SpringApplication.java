package com.kafka.consumer2spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Consumer2SpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(Consumer2SpringApplication.class, args);
	}
}
