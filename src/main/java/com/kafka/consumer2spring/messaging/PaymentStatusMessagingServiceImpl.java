package com.kafka.consumer2spring.messaging;

import com.kafka.consumer2spring.dto.response.PaymentStatusMessageResponseDTO;
import com.kafka.consumer2spring.messaging.schema.PaymentStatusMessage;
import com.kafka.consumer2spring.messaging.storage.MessageStorage;
import com.kafka.consumer2spring.service.PaymentStatusMessagingService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class PaymentStatusMessagingServiceImpl implements PaymentStatusMessagingService {

    @Autowired
    MessageStorage messageStorage;

    Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public List<PaymentStatusMessageResponseDTO> getMessages() {
        return messageStorage.getStorage().stream()
                .map(PaymentStatusMessageResponseDTO::build)
                .collect(toList());
    }

    @Override
    public void clearCache() {
        this.messageStorage.clear();
    }


    @KafkaListener(
            topics = "${kafka.topic}"
    )
    public void listen(ConsumerRecord<String, PaymentStatusMessage> record) {
        log.debug("Received message Offset = {}, Key = {}, Value = {} from partition {}",
                record.offset(), record.key(), record.value(), record.partition());
        messageStorage.add(record);
    }
}
